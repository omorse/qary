
# Quizbot Web API roadmap

Ideas about how to develop api interface for qary and landbot.io in an agile way (for Syndee and Blankpage). 

Main challenges / assumptions: 
* The response received by landbot.io needs to be flat (no nested structures)
* Landbot needs to know how many elements it receives
* Output formats Syndee needs: message/s+free text reply, message/s + buttons, image, madlibs, rating, finish conversation

## Request format 
**Option 1:** (Probably easier)
`POST api.qary.ai`
Request body:
```
{
   user_id: <user_id>,
   bot_name: <bot_name>,
   type: "text",
   text: "hey"
}
```

 
**Option 2:** (Inspired by [Botpress's Converse API](https://botpress.com/docs/channels/converse))
POST api.qary.ai/<bot-name>/<user_id>
Request body:
```
{
   type: "text",
   text: "hey"
}
```

## MVP 1.0: Single message response
Probably enough for initial Blankpage demo. 
```
{
   type: "single_message_freetext"
   message_content_1: "<message_content>"
}
```

```
{
   type: "end_conversation"
   message_content_1: "<message_content>"
}
```

### MVP 2.0: Multiple messages response

Multiple messages response: 
```
{
   type: "multiple_message_freetext"
   num_messages: <num_messages> #number up to 4
   message_content_1: "<message_content>"
   message_content_2: "<message_content>"
   message_content_3: "<message_content>"
   message_content_4: "<message_content>"
}
```

Ability to receive a request to restart conversation (and reset the bot state to starting state):
```
{
   type: "restart",
}
```

### MVP 3.0: Buttons
```
{
   type: "multiple_message_buttons"
   num_messages: <num_messages> #number up to 4
   num_buttons: <num_buttons> #number up to 6
   message_content_1: "<message_content>"
   message_content_2: "<message_content>"
   message_content_3: "<message_content>"
   message_content_4: "<message_content>"
   button_1: "<button_content>"
   button_2: "<button_content>"
   button_3: "<button_content>"
   button_4: "<button_content>"
   button_5: "<button_content>"
   button_6: "<button_content>"
}
```

### MVP 4.0: Multiple Message Types 
```
{
   type: "multiple_message_buttons"
   num_messages: <num_messages> #number up to 4
   num_buttons: <num_buttons> #number up to 6
   message_type_1: "text"
   message_content_1: "<message_content>"
   message_type_2: "image"
   message_content_2: "<message_content>"
   message_type_3: ""
   message_content_3: "<message_content>"
   message_type_4: ""
   message_content_4: "<message_content>"
   button_1: "<button_content>"
   button_2: "<button_content>"
   button_3: "<button_content>"
   button_4: "<button_content>"
   button_5: "<button_content>"
   button_6: "<button_content>"
}
```

### MVP 5.0: madlibs
Response: 
```
{
    type: "madlibs"
    num_questions: <num_questions>
    question_content_1: <question_content>
    question_content_2: <question_content>
    question_content_3: <question_content> 
}
```

Request with madlibs question answers: 
